import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import { createError } from '../error.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const createFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, '..', 'files', req.body.filename);
    if (!req.body.content || !req.body.filename || !req.body.password) {
      next(
        createError(
          400,
          `Please specify ${
            req.body.content ? 'filename' : 'content'
          } parameter`
        )
      );
    } else {
      if (fs.existsSync(filePath)) {
        next(createError(400, 'File already exists!'));
      }
      fs.writeFile(
        filePath,
        JSON.stringify({
          content: req.body.content,
          password: req.body.password,
        }),
        (err) => {
          if (err)
            res.json({
              message: err.message,
            });
          res.status(200).json({
            message: 'File created successfully',
          });
        }
      );
    }
  } catch (error) {
    next(createError(500, 'Server error'));
  }
};
