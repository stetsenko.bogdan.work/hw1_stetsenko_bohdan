import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import { createError } from '../error.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const returnResult = (filename, content, filePath) => {
  return {
    message: 'Success',
    filename: filename,
    content: content,
    extension: path.extname(filePath),
    uploadedDate: fs.statSync(filePath).birthtime,
  };
};
function IsJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
export const getFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, '..', 'files', req.params.name);
    if (!fs.existsSync(filePath)) {
      next(
        createError(400, `No file with '${req.params.name}' filename found`)
      );
    } else {
      const obj = fs.readFileSync(filePath, 'utf8');
      console.log(obj);
      if (!IsJsonString(obj)) {
        res.status(200).json(returnResult(req.params.name, obj, filePath));
      } else if (IsJsonString(obj)) {
        const { password, content } = JSON.parse(
          fs.readFileSync(filePath, 'utf8')
        );
        if (req.query.pass && req.query.pass === password) {
          res
            .status(200)
            .json(returnResult(req.params.name, content, filePath));
        } else {
          next(createError(400, 'Wrong password!'));
        }
      } else {
        throw new Error();
      }
    }
  } catch (error) {
    next(createError(500, 'Server error'));
  }
};
