import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import { createError } from '../error.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const getFiles = (req, res, next) => {
  try {
    const dirPath = path.join(__dirname, '..', 'files');
    const findDir = fs.opendirSync(dirPath);
    if (!findDir) {
      res.status(400).json({
        message: 'Client error',
      });
    } else {
      res
        .status(200)
        .json({ message: 'Success', files: fs.readdirSync(dirPath) });
    }
  } catch (error) {
    next(createError(500, 'Server error'));
  }
};
