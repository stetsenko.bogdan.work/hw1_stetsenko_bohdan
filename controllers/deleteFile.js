import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import { createError } from '../error.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const deleteFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, '..', 'files', req.params.name);
    if (!fs.existsSync(filePath)) {
      next(createError(400, `No file with '${req.params.name}' filename found`));
    } else {
      fs.unlinkSync(filePath);
      res.status(200).json({
        message: 'File was deleted!',
        filename: req.params.name,
        extension: path.extname(filePath),
      });
    }
  } catch (error) {
   next(createError(500, 'Server error'));
  }
};
