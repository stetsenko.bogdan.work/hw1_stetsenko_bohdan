import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import { createError } from '../error.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const modifyFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, '..', 'files', req.body.filename);
    if (!fs.existsSync(filePath)) {
      res.status(400).json({
        message: `No file with '${req.body.filename}' filename found`,
      });
    } else {
      fs.writeFileSync(filePath, req.body.content);
      res.status(200).json({
        message: 'File was updated!',
        filename: req.body.filename,
        newContent: req.body.content,
      });
    }
  } catch (error) {
    next(createError(500, 'Server error'));
  }
};
