import express from 'express';
import { createFile } from '../controllers/createFile.js';
import { deleteFile } from '../controllers/deleteFile.js';
import { getFile } from '../controllers/getFile.js';
import { getFiles } from '../controllers/getFiles.js';
import { modifyFile } from '../controllers/modifyFile.js';

const router = express.Router();

//create file
router.post('/files', createFile);

//get files
router.get('/files', getFiles);

//get file by filename
router.get('/files/:name', getFile);

//delete file
router.delete('/files/:name', deleteFile);

//modify file
router.put('/files', modifyFile);

export default router;
